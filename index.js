let myTrainer = {
    name: 'Conrad Levasty',
    age: 23,
    pokemon: ['Rayquaza', 'Kyogre', 'Groudon'],
    friends: {

        hoenn: ['May', 'Max'],
        kanto: ['Misty', 'Brock'],
    },
    talk: function(){
        console.log('Pikachu! I choose you!')
    }

}
console.log(myTrainer);
console.log(typeof myTrainer);

console.log("Result of dot notation:")

console.log(myTrainer.name);
console.log(myTrainer.age);
console.log(myTrainer['pokemon']);
console.log(myTrainer['friends']);

console.log("Result of talk method:")
myTrainer.talk();

/* let myPokemon = {
    name: 'Dialga',
    level: 70,
    health: 2 * level,
    attack: level,
    tackle: function(){
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduce to _tagetPokemonHealth");
    },
    faint: function(){
        console.log("Pokemon fainted");
    }
 }*/

 function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    

    this.tackle = function(target){
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
        damageHp = (target.health - this.attack);
        target.health = damageHp
        if(damageHp <= 0){
            target.faint();
        }

    };
    this.faint = function(){
        console.log(this.name + " fainted");
    };

 }   


let dialga = new Pokemon("Dialga", 50);
let giratina = new Pokemon("Giratina", 60);
let magicarp = new Pokemon("Magicarp", 1);

console.log(dialga);
console.log(giratina);
console.log(magicarp);

dialga.tackle(magicarp);
dialga.tackle(giratina);

console.log(magicarp);

